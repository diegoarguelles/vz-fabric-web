define('app',['exports', 'aurelia-router'], function (exports, _aureliaRouter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.App = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var App = exports.App = function () {
    function App() {
      _classCallCheck(this, App);

      this.message = 'Hello World!';
    }

    App.prototype.configureRouter = function configureRouter(config, router) {
      config.title = 'VZ Fabric';

      config.map([{
        route: ['', 'microservices'], name: 'microservices',
        moduleId: './microservice/dashboard/microservice-dashboard', nav: true, title: 'Microservices'
      }, {
        route: 'microservice/create', name: 'newMicroservice',
        moduleId: './microservice/form/microservice-form', nav: true, title: 'New Microservice'
      }, {
        route: 'microservice/:serviceId', name: 'Microservice',
        moduleId: './microservice/view/microservice-view', title: 'Microservice'
      }]);

      this.router = router;
    };

    return App;
  }();
});
define('environment',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    debug: true,
    testing: true
  };
});
define('main',['exports', './environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.configure = configure;

  var _environment2 = _interopRequireDefault(_environment);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function configure(aurelia) {
    aurelia.use.standardConfiguration().feature('resources');

    aurelia.use.plugin('aurelia-materialize-bridge', function (b) {
      return b.useAll();
    });

    if (_environment2.default.debug) {
      aurelia.use.developmentLogging();
    }

    if (_environment2.default.testing) {
      aurelia.use.plugin('aurelia-testing');
    }

    aurelia.start().then(function () {
      return aurelia.setRoot();
    });
  }
});
define('config/client',['exports', 'aurelia-framework', 'aurelia-fetch-client'], function (exports, _aureliaFramework, _aureliaFetchClient) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Client = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _dec, _class;

  var Client = exports.Client = (_dec = (0, _aureliaFramework.inject)(_aureliaFetchClient.HttpClient), _dec(_class = function () {
    function Client(http) {
      _classCallCheck(this, Client);

      this.http = http;
    }

    Client.prototype.getFrom = function getFrom(uri) {
      return this.http.fetch(uri);
    };

    Client.prototype.postTo = function postTo(uri, element) {
      return this.http.fetch(uri, {
        method: 'post',
        body: (0, _aureliaFetchClient.json)(element)
      });
    };

    Client.prototype.putTo = function putTo(uri, element) {
      return this.http.fetch(uri, {
        method: 'put',
        body: (0, _aureliaFetchClient.json)(element)
      });
    };

    Client.prototype.deleteFrom = function deleteFrom(uri) {
      return this.http.fetch(uri, {
        method: 'delete'
      });
    };

    return Client;
  }()) || _class);
});
define('model/microservice',['exports', './platform-setting-java'], function (exports, _platformSettingJava) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Microservice = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Microservice = exports.Microservice = function Microservice(attributes) {
    _classCallCheck(this, Microservice);

    this.id = attributes.id;
    this.vzId = attributes.vzId;
    this.replaceRepository = attributes.replaceRepository;
    this.platform = attributes.platform;
    this.projectType = attributes.projectType;
    this.pipeline = attributes.pipeline;
    this.legacyUrl = attributes.legacyUrl;
    this.deploymentLinks = attributes.deploymentLinks;
    this.platformSettings = new _platformSettingJava.PlatformSettingsJava(attributes.platformSettings);
  };
});
define('model/platform-setting-java',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var PlatformSettingsJava = exports.PlatformSettingsJava = function PlatformSettingsJava(settings) {
    _classCallCheck(this, PlatformSettingsJava);

    this.groupId = settings.groupId;
    this.artifactId = settings.artifactId;
    this.version = settings.version;
    this.jenkinsUrl = settings.jenkinsUrl;
    this.jenkinsCredentials = settings.jenkinsCredentials;
    this.onestashUrl = settings.onestashUrl;
    this.onestashCredentials = settings.onestashCredentials;
  };
});
define('resources/index',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.configure = configure;
  function configure(config) {}
});
define('util/base-component',[], function () {
  "use strict";
});
define('util/constants',[], function () {
  "use strict";
});
define('util/date-format',['exports', 'moment'], function (exports, _moment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.DateFormatValueConverter = undefined;

  var _moment2 = _interopRequireDefault(_moment);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var DateFormatValueConverter = exports.DateFormatValueConverter = function () {
    function DateFormatValueConverter() {
      _classCallCheck(this, DateFormatValueConverter);
    }

    DateFormatValueConverter.prototype.toView = function toView(value) {
      return (0, _moment2.default)(value).format('M/D/YYYY h:mm:ss a');
    };

    return DateFormatValueConverter;
  }();
});
define('layout/nav-bar/nav-bar',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var NavBar = exports.NavBar = function NavBar() {
    _classCallCheck(this, NavBar);
  };
});
define('microservice/client/jenkins-client',['exports', 'aurelia-framework', 'aurelia-fetch-client'], function (exports, _aureliaFramework, _aureliaFetchClient) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.JenkinsClient = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _dec, _class;

  var JenkinsClient = exports.JenkinsClient = (_dec = (0, _aureliaFramework.inject)(_aureliaFetchClient.HttpClient), _dec(_class = function () {
    function JenkinsClient(http) {
      _classCallCheck(this, JenkinsClient);

      this.http = http;
    }

    JenkinsClient.prototype.get = function get(jenkinsUrl, credentials) {
      var authToken = 'Basic ' + window.btoa(credentials.username + ':' + credentials.password);
      return this.http.fetch(jenkinsUrl, {
        headers: {
          Authorization: authToken,
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).then(function (response) {
        return response.json();
      });
    };

    return JenkinsClient;
  }()) || _class);
});
define('microservice/client/microservice-client',["exports", "aurelia-framework", "../../config/client"], function (exports, _aureliaFramework, _client) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.MicroserviceClient = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _dec, _class;

  var MicroserviceClient = exports.MicroserviceClient = (_dec = (0, _aureliaFramework.inject)(_client.Client), _dec(_class = function () {
    function MicroserviceClient(client) {
      _classCallCheck(this, MicroserviceClient);

      this.serviceBasePath = 'http://localhost:9000/api';
      this.client = client;
    }

    MicroserviceClient.prototype.saveService = function saveService(microservice) {
      return this.client.postTo(this.serviceBasePath + '/application', microservice).then(function (response) {
        return response.json();
      });
    };

    MicroserviceClient.prototype.getMicroserviceCreationStatus = function getMicroserviceCreationStatus(id) {
      return this.client.getFrom(this.serviceBasePath + '/application/' + id + '/creation-status').then(function (response) {
        return response.json();
      });
    };

    MicroserviceClient.prototype.getPipelineExecutionStatus = function getPipelineExecutionStatus(serviceId) {
      return this.client.getFrom(this.serviceBasePath + '/executions-on/' + serviceId).then(function (response) {
        return response.json();
      });
    };

    MicroserviceClient.prototype.getService = function getService(serviceId) {
      return this.client.getFrom(this.serviceBasePath + '/application/' + serviceId).then(function (response) {
        return response.json();
      });
    };

    MicroserviceClient.prototype.getAllServices = function getAllServices() {
      return this.client.getFrom(this.serviceBasePath + '/applications').then(function (response) {
        return response.json();
      });
    };

    MicroserviceClient.prototype.executePipelineFor = function executePipelineFor(appId) {
      return this.client.postTo(this.serviceBasePath + '/execute-pipeline-on/' + appId, null).then(function (response) {
        return response.json();
      });
    };

    return MicroserviceClient;
  }()) || _class);
});
define('microservice/dashboard/microservice-dashboard',['exports', 'aurelia-framework', 'moment', '../services/microservice-service', 'aurelia-router'], function (exports, _aureliaFramework, _moment, _microserviceService, _aureliaRouter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.MicroserviceDashboard = undefined;

  var _moment2 = _interopRequireDefault(_moment);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _dec, _class;

  var MicroserviceDashboard = exports.MicroserviceDashboard = (_dec = (0, _aureliaFramework.inject)(_microserviceService.MicroserviceService, _aureliaRouter.Router), _dec(_class = function () {
    function MicroserviceDashboard(microserviceService, router) {
      _classCallCheck(this, MicroserviceDashboard);

      this.microserviceService = microserviceService;
      this.router = router;
      this.microservices = [];
    }

    MicroserviceDashboard.prototype.attached = function attached() {
      var _this = this;

      this.microserviceService.getAllServices().then(function (response) {
        console.log(response);
        _this.microservices = response;
      });
    };

    MicroserviceDashboard.prototype.goToService = function goToService(serviceId) {
      this.router.navigate('microservice/' + serviceId);
    };

    return MicroserviceDashboard;
  }()) || _class);
});
define('microservice/form/microservice-form',['exports', 'aurelia-framework', 'aurelia-materialize-bridge', '../services/microservice-service', '../../model/platform-setting-java', '../../model/microservice', 'aurelia-router'], function (exports, _aureliaFramework, _aureliaMaterializeBridge, _microserviceService, _platformSettingJava, _microservice, _aureliaRouter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.MicroserviceForm = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _dec, _class;

  var MicroserviceForm = exports.MicroserviceForm = (_dec = (0, _aureliaFramework.inject)(_microserviceService.MicroserviceService, _aureliaMaterializeBridge.MdToastService, _aureliaRouter.Router), _dec(_class = function () {
    MicroserviceForm.prototype.selectServiceType = function selectServiceType(value) {
      var FABRIC = 'FABRIC';
      var LEGACY = 'LEGACY';

      if (value == FABRIC) {
        this.showOptions = true;
        this.showLegacyAppOption = false;
        this.showNewAppOption = true;
      }

      if (value == LEGACY) {
        this.showOptions = true;
        this.showNewAppOption = false;
        this.showLegacyAppOption = true;
      }
    };

    function MicroserviceForm(microserviceService, toast, router) {
      _classCallCheck(this, MicroserviceForm);

      this.showOptions = false;
      this.showNewAppOption = false;
      this.showLegacyAppOption = false;
      this.selectedPullOption = "";
      this.selectedBuildOption = "";
      this.selectedDeployEnvOption = "";
      this.selectedTestOption = [];
      this.testOptions = [{
        tool: "JUNIT",
        imgUrl: "http://www.codeaffine.com/wp-content/uploads/2016/02/junit-lambda-300x102.png"
      }, {
        tool: "SELENIUM",
        imgUrl: "https://1.bp.blogspot.com/-QOT1qGdvL0Y/V-rv1MnVoNI/AAAAAAAAHa4/5hgkmQd_N7UysprgAaSKDADsv6yvyzInwCPcB/s200/Seleniumlogo.png"
      }];

      this.router = router;
      this.microserviceService = microserviceService;
      this.toast = toast;
      this.showLoader = false;
      this.loaderValue = "0";
      this.loaderMessage = "In progress";
      this.showSpinner = false;
      this.microservice = {};
      this.microservice.version = '1.0.0-SNAPSHOT';
    }

    MicroserviceForm.prototype.createPipelineString = function createPipelineString() {

      var pipelineArray = [];
      pipelineArray.push(this.selectedPullOption);
      pipelineArray.push(this.selectedBuildOption);
      pipelineArray.push(this.selectedDeployEnvOption);

      for (var i = 0; i < this.selectedTestOption.length; i++) {
        pipelineArray.push(this.selectedTestOption[i].tool);
      }

      var pipelineString = pipelineArray.join(',');
      return pipelineString;
    };

    MicroserviceForm.prototype.saveService = function saveService() {
      var _this = this;

      if (this.isValidMicroserviceInformation()) {
        var serviceToSave = {};

        serviceToSave.id = this.microservice.id;
        serviceToSave.vzId = "ARGUDI7";
        serviceToSave.replaceRepository = "0";
        serviceToSave.platform = 'JAVA';
        serviceToSave.projectType = this.microservice.serviceType;
        serviceToSave.legacyUrl = this.microservice.legacyUrl;
        serviceToSave.platformSettings = this.getPlatformSetting(this.microservice.serviceType);
        serviceToSave.pipeline = this.createPipelineString();

        this.showToast('Creating service...');

        this.showSpinner = true;

        this.microserviceService.saveService(new _microservice.Microservice(serviceToSave)).then(function (response) {
          if (!response) {
            _this.showToast('An error occured');
          } else {
            var id = response.id;
            _this.showToast('Service created');
            _this.showLoader = true;
            _this.navigateTo('microservice/' + id);
          }
        });
      } else {
        this.showSpinner = false;
        this.showToast('Incomplete data');
      }
    };

    MicroserviceForm.prototype.checkCreationStatus = function checkCreationStatus(microserviceId) {
      var _this2 = this;

      if (this.loaderValue == '100') {
        console.log('Redirecting....');
        this.router.navigate('microservice/' + microserviceId);
      }
      this.microserviceService.getMicroserviceCreationStatus(microserviceId).then(function (response) {
        console.log('RESPONSE : ' + response);
        if ('message' in response) {
          _this2.showToast('An error occured');
        } else {
          _this2.loaderValue = response.percentage.replace('%', '');
          _this2.loaderMessage = response.status;
          setTimeout(null, 5000);
          _this2.checkCreationStatus(microserviceId);
        }
      });
    };

    MicroserviceForm.prototype.isValidMicroserviceInformation = function isValidMicroserviceInformation() {
      if (this.microservice.serviceType == "JAVA") {
        return this.microservice.groupId !== '' && this.microservice.artifactId !== '' && this.microservice.version !== '' && this.microservice.serviceType !== '';
      } else {
        return this.microservice.legacyUrl !== '';
      }
    };

    MicroserviceForm.prototype.getPlatformSetting = function getPlatformSetting(type) {
      var settings = {};
      if (type == 'FABRIC') {
        settings.groupId = this.microservice.groupId;
        settings.artifactId = this.microservice.artifactId;
        settings.version = this.microservice.version;

        settings.jenkinsUrl = 'http://localhost:8080/';
        settings.jenkinsCredentials = { username: 'murruer', password: 'murruer' };
        settings.onestashUrl = "https://onestash.verizon.com";
        settings.onestashCredentials = { username: 'murruer', password: 'egipto2017.' };

        return new _platformSettingJava.PlatformSettingsJava(settings);
      }

      if (type == 'LEGACY') {
        settings.artifactId = this.microservice.artifactId;
        settings.version = this.microservice.version;
        settings.jenkinsUrl = 'http://localhost:8080/';
        settings.jenkinsCredentials = { username: 'murruer', password: 'murruer' };
        settings.onestashUrl = this.microservice.legacyUrl;
        settings.onestashCredentials = { username: 'murruer', password: 'egipto2017.' };

        return new _platformSettingJava.PlatformSettingsJava(settings);
      }
    };

    MicroserviceForm.prototype.showToast = function showToast(message) {
      this.toast.show(message, 4000);
    };

    MicroserviceForm.prototype.navigateTo = function navigateTo(route) {
      this.router.navigate(route);
    };

    MicroserviceForm.prototype.extractArtifactIdFromLegacyUrl = function extractArtifactIdFromLegacyUrl(url) {
      if (url) {
        var urlSplitted = url.split('.git');
        console.log(urlSplitted);
        var locacion = urlSplitted[0];
        console.log(locacion);
        var locationSplitted = locacion.split('/');
        var project = locationSplitted[locationSplitted.length - 1];
        console.log(project);
        this.microservice.artifactId = project;
      }
    };

    return MicroserviceForm;
  }()) || _class);
});
define('microservice/view/microservice-view',['exports', 'aurelia-framework', 'aurelia-materialize-bridge', '../services/microservice-service', '../services/jenkins-service'], function (exports, _aureliaFramework, _aureliaMaterializeBridge, _microserviceService, _jenkinsService) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.MicroserviceView = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _dec, _class;

  var MicroserviceView = exports.MicroserviceView = (_dec = (0, _aureliaFramework.inject)(_microserviceService.MicroserviceService, _aureliaMaterializeBridge.MdToastService, _jenkinsService.JenkinsService), _dec(_class = function () {
    function MicroserviceView(microserviceService, toast, jenkinsService) {
      _classCallCheck(this, MicroserviceView);

      this.showJavaPlatformInfo = false;
      this.showLegacyInfo = false;
      this.loaderValue = 0;
      this.buildStatus = 'FAILED';
      this.totalBuildTime = 0;
      this.buildIcon = '';
      this.showPipelineStatusIcon = false;

      this.microservice = {};
      this.pipelines = [];
      this.microserviceService = microserviceService;
      this.jenkinsService = jenkinsService;
      this.toast = toast;
      this.isLoadingData = true;
      this.showLoader = true;
    }

    MicroserviceView.prototype.activate = function activate(paramsFromUrl) {
      this.serviceId = paramsFromUrl.serviceId;
    };

    MicroserviceView.prototype.attached = function attached() {
      var _this = this;

      this.microserviceService.getService(this.serviceId).then(function (response) {
        _this.microservice = response;
        _this.loaderValue = response.fabricStatus.percentage;
        if (_this.microservice.platform == 'JAVA') {
          _this.showLegacyInfo = false;
          _this.showJavaPlatformInfo = true;
        }
        if (_this.microservice.platform == 'LEGACY') {
          _this.showJavaPlatformInfo = false;
          _this.showLegacyInfo = true;
        }
      });

      setTimeout(function () {
        _this.processProgressBarValue(_this.microservice.id);
      }, 2000);

      setTimeout(function () {
        _this.processPipelineCreation(_this.microservice.id);
      }, 2000);
    };

    MicroserviceView.prototype.processProgressBarValue = function processProgressBarValue(appId) {
      var _this2 = this;

      if (this.microservice.fabricStatus.percentage == '100' || this.microservice.fabricStatus.status == 'FAILED') {
        return false;
      }
      this.getServiceCreationStatus(appId);

      setTimeout(function () {
        _this2.processProgressBarValue(appId);
      }, 5000);
    };

    MicroserviceView.prototype.getServiceCreationStatus = function getServiceCreationStatus(appId) {
      var _this3 = this;

      this.microserviceService.getMicroserviceCreationStatus(appId).then(function (response) {
        _this3.loaderValue = response.percentage;
        _this3.microservice.fabricStatus.percentage = response.fabricStatus.percentage;
        _this3.microservice.fabricStatus.status = response.fabricStatus.status;
      });
    };

    MicroserviceView.prototype.processPipelineCreation = function processPipelineCreation(appId) {
      var _this4 = this;

      this.getPipelineCreationStatus(appId);
      setTimeout(function () {
        _this4.processPipelineCreation(appId);
      }, 10000);
    };

    MicroserviceView.prototype.getPipelineCreationStatus = function getPipelineCreationStatus() {
      var _this5 = this;

      var pipelineArray = void 0;
      var jenkinsPipelineUrl = this.getJenkinsURLFromMicroservice();
      var credentials = this.microservice.platformSettings.jenkinsCredentials;
      this.jenkinsService.getPipelineExecutionStatus(jenkinsPipelineUrl, credentials).then(function (response) {
        pipelineArray = response;

        pipelineArray.forEach(function (p) {
          _this5.selectCardStyleForPipelineStep(p);
        });

        if (_this5.isPipelineExecutionFinished(pipelineArray)) {
          _this5.selectBuildStatusIcon(pipelineArray[0].stages);
        }

        _this5.pipelines = pipelineArray;
      });
    };

    MicroserviceView.prototype.getJenkinsURLFromMicroservice = function getJenkinsURLFromMicroservice() {
      var jenkinsURL = '';
      this.microservice.deploymentLinks.forEach(function (l) {
        if (l.key == "pipeline") {
          jenkinsURL = l.uri;
        }
      });
      return jenkinsURL;
    };

    MicroserviceView.prototype.isPipelineExecutionFinished = function isPipelineExecutionFinished(pipelineArray) {
      return pipelineArray[0].status == 'SUCCESS' || pipelineArray[0].status == 'FAILED';
    };

    MicroserviceView.prototype.selectBuildStatusIcon = function selectBuildStatusIcon(pipelineStages) {
      var _this6 = this;

      pipelineStages.forEach(function (s) {
        if (s.status == 'SUCCESS') {
          _this6.showPipelineStatusIcon = true;
          _this6.buildStatus = 'OK';
          _this6.buildIcon = 'http://findicons.com/files/icons/2603/weezle/256/weezle_sun.png';
        }
        if (s.status == 'FAILED') {
          _this6.buildStatus = 'FAILED';
          _this6.buildIcon = 'https://openclipart.org/image/2400px/svg_to_png/30163/weather-storm.png';
          _this6.showPipelineStatusIcon = true;
        }
      });
    };

    MicroserviceView.prototype.selectCardStyleForPipelineStep = function selectCardStyleForPipelineStep(pipeline) {
      pipeline.stages.forEach(function (s) {
        if (s.status == 'SUCCESS') {
          s.class = 'card-pipeline-completed';
        }
        if (s.status == 'PENDING') {
          s.class = 'card-pipeline-empty';
        }
        if (s.status == 'FAILED') {
          s.class = 'card-pipeline-error';
        }
      });
    };

    MicroserviceView.prototype.executePipeline = function executePipeline() {
      var _this7 = this;

      this.microserviceService.executePipelineFor(this.serviceId).then(function (response) {
        _this7.showPipelineStatusIcon = false;
      });
    };

    MicroserviceView.prototype.showDefaultToast = function showDefaultToast(message) {
      this.toast.show(message, 4000);
    };

    return MicroserviceView;
  }()) || _class);
});
define('microservice/services/jenkins-service',['exports', 'aurelia-framework', '../client/jenkins-client', 'aurelia-fetch-client'], function (exports, _aureliaFramework, _jenkinsClient, _aureliaFetchClient) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.JenkinsService = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _dec, _class;

  var JenkinsService = exports.JenkinsService = (_dec = (0, _aureliaFramework.inject)(_jenkinsClient.JenkinsClient), _dec(_class = function () {
    function JenkinsService(client) {
      _classCallCheck(this, JenkinsService);

      this.client = client;
    }

    JenkinsService.prototype.getPipelineExecutionStatus = function getPipelineExecutionStatus(jenkinsUrl, credentials) {
      return this.client.get(jenkinsUrl + '/wfapi/runs', credentials).then(function (response) {
        return response;
      }).catch(function (error) {
        console.log(error);
        return false;
      }).then(function (response) {
        return response;
      });
    };

    return JenkinsService;
  }()) || _class);
});
define('microservice/services/microservice-service',['exports', 'aurelia-framework', '../client/microservice-client'], function (exports, _aureliaFramework, _microserviceClient) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.MicroserviceService = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _dec, _class;

  var MicroserviceService = exports.MicroserviceService = (_dec = (0, _aureliaFramework.inject)(_microserviceClient.MicroserviceClient), _dec(_class = function () {
    function MicroserviceService(microserviceClient) {
      _classCallCheck(this, MicroserviceService);

      this.microserviceClient = microserviceClient;
    }

    MicroserviceService.prototype.saveService = function saveService(microservice) {
      return this.microserviceClient.saveService(microservice).then(function (response) {
        return response;
      }).catch(function (error) {
        return false;
      }).then(function (response) {
        return response;
      });
    };

    MicroserviceService.prototype.getMicroserviceCreationStatus = function getMicroserviceCreationStatus(id) {
      return this.microserviceClient.getMicroserviceCreationStatus(id).then(function (response) {
        return response;
      }).catch(function (error) {
        console.log(error);
        return false;
      }).then(function (response) {
        console.log(response);
        return response;
      });
    };

    MicroserviceService.prototype.getPipelineExecutionStatus = function getPipelineExecutionStatus(id) {
      return this.microserviceClient.getPipelineExecutionStatus(id).then(function (response) {
        return response;
      }).catch(function (error) {
        console.log(error);
        return false;
      }).then(function (response) {
        console.log(response);
        return response;
      });
    };

    MicroserviceService.prototype.getService = function getService(id) {
      return this.microserviceClient.getService(id).then(function (response) {
        return response;
      }).catch(function (error) {
        console.log(error);
        return false;
      }).then(function (response) {
        console.log(response);
        return response;
      });
    };

    MicroserviceService.prototype.getAllServices = function getAllServices() {
      return this.microserviceClient.getAllServices().then(function (response) {
        return response;
      }).catch(function (error) {
        console.log(error);
        return false;
      }).then(function (response) {
        console.log(response);
        return response;
      });
    };

    MicroserviceService.prototype.executePipelineFor = function executePipelineFor(appId) {
      return this.microserviceClient.executePipelineFor(appId).then(function (response) {
        return response;
      }).catch(function (error) {
        console.log(error);
        return false;
      }).then(function (response) {
        console.log(response);
        return response;
      });
    };

    return MicroserviceService;
  }()) || _class);
});
define('text!app.html', ['module'], function(module) { module.exports = "\n<template class=\"vzBackground\">\n  <require from=\"materialize-css/css/materialize.css\"></require>\n  <require  from = \"./layout/nav-bar/nav-bar\"></require >\n\n  <md-colors md-primary-color=\"firebrick\" ></md-colors>\n\n  <nav-bar></nav-bar>\n\n  <main class=\"centeredContent\">\n    <router-view></router-view>\n  </main>\n\n</template>\n"; });
define('text!microservice/dashboard/microservice-dashboard.html', ['module'], function(module) { module.exports = "<template>\n  <require from=\"../../util/date-format\"></require>\n\n  <h4>ETMS Services</h4>\n\n  <div style=\"max-width: 1600px\">\n\n    <div class=\"fixed-action-btn horizontal\"\n         style=\"position: absolute; display: inline-block; right: 194px; top: 80px;\">\n      <a md-button=\"floating: true; large: true;\" md-tooltip=\"position: right; text: Create microservice;\"\n         md-waves=\"color: red; circle: true;\"\n         class=\"primary\" route-href=\"route: newMicroservice;\">\n        <i class=\"large material-icons\">add</i>\n      </a>\n    </div>\n\n    <div class=\"row col-lg-12\">\n\n\n      <div repeat.for=\"service of microservices\">\n        <md-card class=\"col s3\" md-title=\"${service.platformSettings.artifactId}\" md-size=\"small\" md-reveal=\"true\"\n                 md-image=\"http://www.tuprogramaras.com/wp-content/uploads/2014/04/java.jpg\">\n          <div>\n            <!--<b>${service.id}</b> <br/>-->\n            <b>${service.platform}</b> <br/>\n            Created on ${service.creationDate | dateFormat}\n          </div>\n          <div slot=\"reveal-text\">\n            <b>Created by</b> ${service.vzId} <br/>\n            <b>Fabric status</b> ${service.fabricStatus.status}<br/>\n            <b>Creation status</b> ${service.fabricStatus.percentage}%<br/>\n\n            <br/>\n            <br/>\n\n            <div class=\"button-row\">\n              <button md-button class=\"primary\" click.delegate=\"goToService(service.id)\">\n                <i class=\"left material-icons\">keyboard_arrow_right</i>\n                More Info\n              </button>\n            </div>\n\n          </div>\n        </md-card>\n      </div>\n\n\n      <!--<md-card class=\"col s3\" md-title=\"vz-ebonding-command\" md-size=\"small\" md-reveal=\"true\" md-image=\"http://blogs.vmware.com/cloudnative/files/2016/10/WP-Pivotal-Cloud-Foundry-620x410.png\">-->\n      <!--<div>-->\n      <!--<b>JAVA</b> <br/>-->\n      <!--Created on 12/12/2012-->\n      <!--</div>-->\n      <!--<div slot=\"reveal-text\">-->\n      <!--Microservice info (url Deploy, url Jenkins, etc)-->\n      <!--<br/>-->\n      <!--Status :-->\n      <!--<md-switch md-checked.bind=\"checked\"></md-switch>-->\n      <!--</div>-->\n      <!--</md-card>-->\n\n    </div>\n  </div>\n</template>\n"; });
define('text!microservice/view/microservice-view.html', ['module'], function(module) { module.exports = "<template>\n  <require from=\"../../util/date-format\"></require>\n  <div style=\"max-width: 1600px\">\n\n    <md-card md-title=\"Service information\">\n\n      <div class=\"fixed-action-btn horizontal\"\n           style=\"position: absolute; display: inline-block; right: 194px; top: 80px;\">\n        <a md-button=\"floating: true; large: true;\" md-tooltip=\"position: right; text: Back to dashboard;\"\n           md-waves=\"color: red; circle: true;\"\n           class=\"primary\" route-href=\"route: microservices;\">\n          <i class=\"large material-icons\">keyboard_backspace</i>\n        </a>\n      </div>\n\n      <!--<md-progress md-type=\"circular\" md-pixel-size.bind=\"pixelSize\" md-size.bind=\"size\" md-color.bind=\"color\"-->\n      <!--if.bind=\"isLoadingData\" class=\"right\">-->\n      <!--</md-progress>-->\n\n      <div class=\"row\">\n\n        <div class=\"col l3 m6 s12\">\n          <h6>Service ID: </h6>\n          <label>${serviceId}</label>\n\n          <h6>OWNER VZID: </h6>\n          <label>${microservice.vzId}</label>\n\n          <h6>PLATFORM: </h6>\n          <img style=\"width: 95px; height: 55px;\"\n               src=\"https://vignette2.wikia.nocookie.net/logopedia/images/6/6a/Java-logo.jpg/revision/latest/scale-to-width-down/640?cb=20150321072347\"/>\n        </div>\n\n        <div class=\"col l3 m6 s12\" if.bind=\"showJavaPlatformInfo\">\n          <h6>GROUP ID: </h6>\n          <label>${microservice.platformSettings.groupId}</label>\n          <h6>ARTIFACT ID: </h6>\n          <label>${microservice.platformSettings.artifactId}</label>\n          <h6>VERSION: </h6>\n          <label>${microservice.platformSettings.version}</label>\n        </div>\n\n        <div class=\"col l3 m6 s12\" if.bind=\"showLegacyInfo\">\n          <h6>LEGACY URL: </h6>\n          <label>${microservice.legacyUrl}</label>\n        </div>\n\n\n        <div class=\"col l3 m6 s12\">\n          <div repeat.for=\"link of microservice.deploymentLinks\" >\n            <h6>URL ${link.key}: </h6>\n            <label>\n              <a href=\"${link.uri}\">${link.uri}</a>\n            </label>\n          </div>\n        </div>\n\n        <div class=\"col l3 m6 s12\">\n\n        </div>\n      </div>\n\n    </md-card>\n\n    <md-card if.bind=\"showLoader\" md-title=\"Creation progress\">\n      <div>\n        <md-progress md-value.bind=\"loaderValue\"></md-progress>\n        ${microservice.fabricStatus.percentage}% - ${microservice.fabricStatus.status}\n      </div>\n    </md-card>\n\n    <md-card md-title=\"Pipeline status\">\n\n      <div class=\"col l3 m6 s12\" if.bind=\"pipelines[0].finished\">\n        <div class=\"button-row\">\n          <button click.delegate=\"executePipeline()\" md-button class=\"primary\"><i class=\"left material-icons\">play_arrow</i>EXECUTE PIPELINE</button>\n        </div>\n      </div>\n\n      <!--Show progress bar when pipeline is executing-->\n      <md-progress md-type=\"circular\" md-size.bind=\"size\" md-color.bind=\"color\"\n                   if.bind=\"!showPipelineStatusIcon\" class=\"right\"></md-progress>\n\n      <!--Show icon of pipeline status when completed-->\n      <h4 class=\"right\" if.bind=\"showPipelineStatusIcon\">\n        <img src=${buildIcon} style=\"width: 50px;height: 50px;\">\n        ${buildStatus}\n      </h4>\n\n      <!--Show message if pipeline is not executed-->\n      <h6 if.bind=\"!showPipelineStatusIcon\">\n        Executing pipeline...\n      </h6>\n\n      <div repeat.for=\"pipeline of pipelines\">\n\n        <div class=\"row\">\n          <div class=\"col l3 m6 s12\">\n            <h6>Execution date: </h6>\n            <label>${pipeline.startTimeMillis | dateFormat}</label>\n          </div>\n          <div class=\"col l3 m6 s12\">\n            <h6>Total duration: </h6>\n            <label>35min</label>\n          </div>\n\n        </div>\n\n        <div class=\"row\">\n\n          <div repeat.for=\"stage of pipeline.stages\">\n            <md-card class=\"col l3 m6 s12 ${stage.class}\" md-title=${stage.name}>\n              <h6>Started at: </h6>\n              <label>${stage.startTimeMillis | dateFormat}</label>\n              <h6>Duration: </h6>\n              <label>${stage.durationMillis} ms</label>\n              <h6>Status: </h6>\n              <label>${stage.status}</label>\n            </md-card>\n          </div>\n\n        </div>\n\n      </div>\n\n      <div style=\"height: 3em\">\n\n      </div>\n    </md-card>\n\n  </div>\n</template>\n"; });
define('text!layout/nav-bar/nav-bar.html', ['module'], function(module) { module.exports = "<template>\n  <md-navbar>\n    <div class=\"centeredContent\">\n      <img class=\"vzLogo\" src=\"http://www.lg.com/us/mobile-phones/v10/img/verizon-desktop.png\">\n      <ul class=\"hide-on-med-and-down right\">\n        <li md-waves><a class=\"headerText\">About</a></li>\n        <li md-waves><a class=\"headerText\">Installation</a></li>\n        <li md-waves><a class=\"headerText\">Project Status</a></li>\n      </ul>\n    </div>\n  </md-navbar>\n</template>\n"; });
define('text!microservice/form/microservice-form.html', ['module'], function(module) { module.exports = "<template>\n  <form submit.delegate=\"saveService()\" novalidate>\n\n    <h4>Create New Service</h4>\n    <md-progress md-type=\"circular\" md-pixel-size.bind=\"pixelSize\" md-size.bind=\"size\" md-color.bind=\"color\"\n                 if.bind=\"showSpinner\" class=\"right\"></md-progress>\n    <br/>\n\n\n    <div class=\"fixed-action-btn horizontal\"\n         style=\"position: absolute; display: inline-block; right: 194px; top: 80px;\">\n      <a md-button=\"floating: true; large: true;\" md-tooltip=\"position: right; text: Back to dashboard;\"\n         md-waves=\"color: red; circle: true;\"\n         class=\"primary\" route-href=\"route: microservices;\">\n        <i class=\"large material-icons\">keyboard_backspace</i>\n      </a>\n    </div>\n\n\n    <select md-select=\"label: Select Service Type\" value.two-way=\"microservice.serviceType\" change.delegate=\"selectServiceType(microservice.serviceType)\">\n      <option value=\"\" disabled selected>Select service type</option>\n      <option value=\"LEGACY\">Legacy Application</option>\n      <option value=\"FABRIC\">Fabric Application</option>\n      <!--<option value=\"NODE\">Node Application</option>-->\n    </select>\n\n    <div if.bind=\"showOptions\" >\n\n      <md-card md-title=\"Main Configuration\" class=\"col s12\" if.bind=\"showNewAppOption\">\n\n        <div class=\"row\">\n\n          <md-input class=\"col s12 m6 l4\" md-label=\"Group ID\" md-value.bind=\"microservice.groupId\"\n                    md-disabled.bind=\"disabledValue\"></md-input>\n          <md-input class=\"col s12 m6 l4\" md-label=\"Artifact ID\" md-value.bind=\"microservice.artifactId\"\n                    md-disabled.bind=\"disabledValue\"></md-input>\n          <md-input class=\"col s12 m6 l4\" md-label=\"Version ID\" md-value.bind=\"microservice.version\"\n                    md-disabled.bind=\"disabledValue\"></md-input>\n        </div>\n      </md-card>\n\n      <md-card md-title=\"Main Configuration\" class=\"col l6 m12 s12\" if.bind=\"showLegacyAppOption\">\n        <div class=\"row\">\n          <md-input class=\"col s12 m12 l6\" md-label=\"REPOSITORY URL\" md-value.bind=\"microservice.legacyUrl\" trigger.bind=\"extractArtifactIdFromLegacyUrl(microservice.legacyUrl)\"></md-input>\n          <md-input class=\"col s12 m12 l3\" md-label=\"Artifact ID\" md-value.bind=\"microservice.artifactId\"\n                    md-disabled.bind=\"disabledValue\"></md-input>\n          <md-input class=\"col s12 m12 l3\" md-label=\"Version ID\" md-value.bind=\"microservice.version\"\n                    md-disabled.bind=\"disabledValue\"></md-input>\n        </div>\n      </md-card>\n\n      <br/>\n\n\n      <div class=\"row\">\n\n        <h5>Pipeline Options</h5>\n        <md-card md-title=\"Pull\" class=\"col s12 l4 m6\">\n          <div class=\"row\">\n            <md-radio class=\"col s12 m12 l6 \" md-name=\"pullProvider\" md-checked.bind=\"selectedPullOption\" md-value=\"GIT\">\n              <img style=\"width: 95px; height: 40px;\"\n                   src=\"https://git-scm.com/images/logos/downloads/Git-Logo-2Color.png\"/>\n            </md-radio>\n            <md-radio class=\"col s12 m12 l6\" md-name=\"pullProvider\" md-checked.bind=\"selectedPullOption\" md-value=\"SVN\">\n              <img style=\"width: 95px; height: 40px;\"\n                   src=\"https://svn.apache.org/repos/asf/subversion/svn-logos/images/tyrus-svn2.png\"/>\n            </md-radio>\n          </div>\n        </md-card>\n\n\n        <md-card md-title=\"Build\" class=\"col s12 l4 m6\">\n          <div class=\"row\">\n            <md-radio class=\"col s12 m12 l6\" md-name=\"buildProvider\" md-checked.bind=\"selectedBuildOption\" md-value=\"MAVEN\">\n              <img style=\"width: 110px; height: 35px;\"\n                   src=\"https://maven.apache.org/images/maven-logo-black-on-white.png\"/>\n            </md-radio>\n            <md-radio class=\"col s12 m12 l6\" md-name=\"buildProvider\" md-checked.bind=\"selectedBuildOption\" md-value=\"GRADLE\">\n              <img style=\"width: 110px; height: 40px;\"\n                   src=\"https://cdn-images-1.medium.com/max/1200/1*gImAplWne6yxtDytygjiYw.png\"/>\n            </md-radio>\n          </div>\n        </md-card>\n\n\n        <md-card md-title=\"Test\" class=\"col s12 l4 m6\">\n          <div class=\"row\">\n            <p repeat.for=\"tool of testOptions\">\n              <md-checkbox class=\"col s12 m12 l6\" md-model.bind=\"tool\" md-checked.bind=\"selectedTestOption\">\n                <img style=\"width: 110px; height: 35px;\" src=\"${tool.imgUrl}\"/>\n              </md-checkbox>\n            </p>\n          </div>\n        </md-card>\n\n\n        <md-card md-title=\"Deploy Environment\" class=\"col s12\">\n          <div class=\"row\">\n\n            <md-radio class=\"col s12 m6 l4\" md-name=\"cloudProvider\" md-checked.bind=\"selectedDeployEnvOption\" md-disabled=\"true\"  md-value=\"AWS\">\n              <img style=\"width: 179px; height: 109px; filter: grayscale(100%)\"\n                   src=\"https://a0.awsstatic.com/main/images/logos/aws_logo_179x109.gif\"/>\n            </md-radio>\n\n            <md-radio class=\"col s12 m6 l4\" md-name=\"cloudProvider\" md-checked.bind=\"selectedDeployEnvOption\"   md-value=\"CLOUD_FOUNDRY\">\n              <img style=\"width: 179px; height: 109px\"\n                   src=\"https://blog.hazelcast.com/wp-content/uploads/2016/01/cloud-foundry.png\"/>\n            </md-radio>\n\n            <md-radio class=\"col s12 m6 l4\" md-name=\"cloudProvider\" md-checked.bind=\"selectedDeployEnvOption\" md-value=\"OWN-SERVER\">\n              <img style=\"height: 109px;\"\n                   src=\"http://1.bp.blogspot.com/-byl39Wd1CIU/VgrTs0lHU6I/AAAAAAAAAew/oMdowX8Xe10/s1600/ico-serv-linux.png\"/>\n            </md-radio>\n\n          </div>\n\n        </md-card>\n\n\n      </div>\n\n\n      <div class=\"button-row\">\n        <button md-button class=\"primary right\"><i class=\"left material-icons\" type=\"submit\">keyboard_arrow_right\n        </i>Create\n        </button>\n      </div>\n\n    </div>\n\n\n  </form>\n</template>\n"; });
define('text!../styles/styles/styles.css', ['module'], function(module) { module.exports = "body {\n  background-color: whitesmoke; }\n\n.headerText {\n  color: whitesmoke;\n  font-size: large; }\n\n.vzLogo {\n  margin-top: -1em;\n  width: 180px;\n  height: 100px; }\n\n.vzBackground {\n  height: 100%; }\n\n.centeredContent {\n  max-width: 100%;\n  width: 1600px;\n  margin-left: auto;\n  margin-right: auto; }\n\n.btn:hover, .btn-large:hover {\n  background-color: #f44336 !important; }\n\n.btn:focus, .btn-large:focus, .btn-floating:focus {\n  outline: none;\n  background-color: firebrick !important; }\n\n.card-pipeline-completed {\n  color: darkgreen; }\n\n.card-pipeline-error {\n  color: darkred; }\n\n.card-pipeline-empty {\n  color: lightgrey; }\n"; });
//# sourceMappingURL=app-bundle.js.map