FROM cstheriot/aurelia-cli
#FROM node:6.9.2

ENV HOME=/var/www

WORKDIR /var/www/

CMD /bin/bash -l bootstrap.sh
