
export class PlatformSettingsJava {

  constructor(settings){
    this.groupId = settings.groupId;
    this.artifactId = settings.artifactId;
    this.version = settings.version;
    this.jenkinsUrl = settings.jenkinsUrl;
    this.jenkinsCredentials = settings.jenkinsCredentials;
    this.onestashUrl = settings.onestashUrl;
    this.onestashCredentials = settings.onestashCredentials;
  }

}
