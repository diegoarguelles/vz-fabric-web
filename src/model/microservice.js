import {PlatformSettingsJava} from './platform-setting-java'

export class Microservice {

  constructor(attributes) {
    this.id = attributes.id;
    this.vzId = attributes.vzId;
    this.replaceRepository = attributes.replaceRepository;
    this.platform = attributes.platform;
    this.projectType = attributes.projectType;
    this.pipeline = attributes.pipeline;
    this.legacyUrl = attributes.legacyUrl;
    this.deploymentLinks = attributes.deploymentLinks;
    this.platformSettings = new PlatformSettingsJava(attributes.platformSettings);
  }

}
