import {inject} from 'aurelia-framework';
import {MdToastService} from 'aurelia-materialize-bridge';
import {MicroserviceService} from '../services/microservice-service';
import {JenkinsService} from '../services/jenkins-service';

@inject(MicroserviceService, MdToastService, JenkinsService)
export class MicroserviceView {

  showJavaPlatformInfo = false;
  showLegacyInfo = false;
  loaderValue = 0;
  buildStatus = 'FAILED'
  totalBuildTime = 0
  buildIcon = ''
  showPipelineStatusIcon = false;

  constructor(microserviceService, toast, jenkinsService) {
    this.microservice = {}
    this.pipelines = []
    this.microserviceService = microserviceService;
    this.jenkinsService = jenkinsService;
    this.toast = toast;
    this.isLoadingData = true;
    this.showLoader = true;
  }

  activate(paramsFromUrl) {
    this.serviceId = paramsFromUrl.serviceId;
  }

  attached() {
    this.microserviceService.getService(this.serviceId)
      .then((response) => {
        this.microservice = response
        this.loaderValue = response.fabricStatus.percentage
        if (this.microservice.platform == 'JAVA') {
          this.showLegacyInfo = false;
          this.showJavaPlatformInfo = true;
        }
        if (this.microservice.platform == 'LEGACY') {
          this.showJavaPlatformInfo = false;
          this.showLegacyInfo = true;
        }
      });

    setTimeout(
      () => {
        this.processProgressBarValue(this.microservice.id);
      },
      2000);

    setTimeout(
      () => {
        this.processPipelineCreation(this.microservice.id);
      },
      2000);
  }

  processProgressBarValue(appId) {

    if (this.microservice.fabricStatus.percentage == '100' || this.microservice.fabricStatus.status == 'FAILED') {
      return false;
    }
    this.getServiceCreationStatus(appId);

    setTimeout(
      () => {
        this.processProgressBarValue(appId);
      },
      5000);
  }

  getServiceCreationStatus(appId) {
    this.microserviceService.getMicroserviceCreationStatus(appId)
      .then(
        (response) => {
          this.loaderValue = response.percentage;
          this.microservice.fabricStatus.percentage = response.fabricStatus.percentage
          this.microservice.fabricStatus.status = response.fabricStatus.status
        }
      )
  }

  processPipelineCreation(appId) {
    this.getPipelineCreationStatus(appId);
    setTimeout(
      () => {
        this.processPipelineCreation(appId);
      },
      10000);
  }

  getPipelineCreationStatus() {
    let pipelineArray;
    let jenkinsPipelineUrl = this.getJenkinsURLFromMicroservice();
    let credentials = this.microservice.platformSettings.jenkinsCredentials;
    this.jenkinsService.getPipelineExecutionStatus(jenkinsPipelineUrl, credentials)
      .then((response) => {
        pipelineArray = response;

        pipelineArray.forEach(p => {
          this.selectCardStyleForPipelineStep(p)
        });

        if (this.isPipelineExecutionFinished(pipelineArray)) {
          this.selectBuildStatusIcon(pipelineArray[0].stages);
        }

        this.pipelines = pipelineArray;

      });
  }

  getJenkinsURLFromMicroservice() {
    let jenkinsURL = '';
    this.microservice.deploymentLinks.forEach(
      l => {
        if (l.key == "pipeline") {
          jenkinsURL = l.uri
        }
      }
    );
    return jenkinsURL;
  }

  isPipelineExecutionFinished(pipelineArray){
    return pipelineArray[0].status == 'SUCCESS' || pipelineArray[0].status == 'FAILED'
  }

  selectBuildStatusIcon(pipelineStages) {
    pipelineStages.forEach(s => {
      if (s.status == 'SUCCESS') {
        this.showPipelineStatusIcon = true
        this.buildStatus = 'OK'
        this.buildIcon = 'http://findicons.com/files/icons/2603/weezle/256/weezle_sun.png'
      }
      if (s.status == 'FAILED') {
        this.buildStatus = 'FAILED'
        this.buildIcon = 'https://openclipart.org/image/2400px/svg_to_png/30163/weather-storm.png'
        this.showPipelineStatusIcon = true
      }
    })
  }

  selectCardStyleForPipelineStep(pipeline) {
    pipeline.stages.forEach(s => {
      if (s.status == 'SUCCESS') {
        s.class = 'card-pipeline-completed';
      }
      if (s.status == 'PENDING') {
        s.class = 'card-pipeline-empty';
      }
      if (s.status == 'FAILED') {
        s.class = 'card-pipeline-error';
      }
    })
  }

  executePipeline() {
    this.microserviceService.executePipelineFor(this.serviceId)
      .then(
        (response) => {
          this.showPipelineStatusIcon = false
        }
      )
  }

  showDefaultToast(message) {
    this.toast.show(message, 4000);
  }

}
