import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';

@inject(HttpClient)
export class JenkinsClient {

  constructor(http) {
    this.http = http;
  }

  get(jenkinsUrl, credentials) {
    const authToken = 'Basic '+window.btoa(credentials.username+':'+credentials.password)
    return this.http.fetch(jenkinsUrl, {
      headers: {
        Authorization: authToken,
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).then(response => response.json());
  }

}
