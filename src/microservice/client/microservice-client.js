import {inject} from "aurelia-framework";
import {Client} from "../../config/client";

@inject(Client)
export class MicroserviceClient {

  constructor(client) {
    this.serviceBasePath = 'http://localhost:9000/api'
    this.client = client;
  }

  saveService(microservice) {
    return this.client.postTo(this.serviceBasePath + '/application', microservice)
      .then(response => response.json())
  }

  getMicroserviceCreationStatus(id) {
    return this.client.getFrom(this.serviceBasePath + '/application/' + id + '/creation-status')
      .then(response => response.json())
  }

  getPipelineExecutionStatus(serviceId) {
    return this.client.getFrom(this.serviceBasePath + '/executions-on/' + serviceId)
      .then(response => response.json())
  }

  getService(serviceId) {
    return this.client.getFrom(this.serviceBasePath + '/application/' + serviceId)
      .then(response => response.json())
  }

  getAllServices() {
    return this.client.getFrom(this.serviceBasePath + '/applications')
      .then(response => response.json())
  }

  executePipelineFor(appId) {
    return this.client.postTo(this.serviceBasePath + '/execute-pipeline-on/' + appId, null)
      .then(response => response.json())
  }

}
