import {inject} from 'aurelia-framework';
import {JenkinsClient} from '../client/jenkins-client'
import {json} from 'aurelia-fetch-client';

@inject(JenkinsClient)
export class JenkinsService {

  constructor(client) {
    this.client = client
  }

  getPipelineExecutionStatus(jenkinsUrl, credentials) {
    return this.client.get(jenkinsUrl + '/wfapi/runs', credentials)
      .then(response => response)
      .catch((error) => {
        console.log(error)
        return false;
      })
      .then((response) => {
          return response
        }
      );
  }

}
