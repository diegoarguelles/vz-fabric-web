import {inject} from 'aurelia-framework';
import {MicroserviceClient} from '../client/microservice-client'

@inject(MicroserviceClient)
export class MicroserviceService {

  constructor(microserviceClient) {
    this.microserviceClient = microserviceClient
  }

  saveService(microservice) {
    return this.microserviceClient.saveService(microservice)
      .then(response => response)
      .catch((error) => {
        return false;
      })
      .then((response) => {
          return response
        }
      )
  }

  getMicroserviceCreationStatus(id) {
    return this.microserviceClient.getMicroserviceCreationStatus(id)
      .then(response => response)
      .catch((error) => {
        console.log(error)
        return false;
      })
      .then((response) => {
          console.log(response)
          return response
        }
      )
  }

  getPipelineExecutionStatus(id) {
    return this.microserviceClient.getPipelineExecutionStatus(id)
      .then(response => response)
      .catch((error) => {
        console.log(error)
        return false;
      })
      .then((response) => {
          console.log(response)
          return response
        }
      )
  }

  getService(id) {
    return this.microserviceClient.getService(id)
      .then(response => response)
      .catch((error) => {
        console.log(error)
        return false;
      })
      .then((response) => {
          console.log(response)
          return response
        }
      )
  }

  getAllServices() {
    return this.microserviceClient.getAllServices()
      .then(response => response)
      .catch((error) => {
        console.log(error)
        return false;
      })
      .then((response) => {
          console.log(response)
          return response
        }
      )
  }

  executePipelineFor(appId) {
    return this.microserviceClient.executePipelineFor(appId)
      .then(response => response)
      .catch((error) => {
        console.log(error)
        return false;
      })
      .then((response) => {
          console.log(response)
          return response
        }
      )
  }

}
