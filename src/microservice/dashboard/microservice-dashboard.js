import {inject} from 'aurelia-framework';
import moment from 'moment';
import {MicroserviceService} from '../services/microservice-service';

import {Router} from 'aurelia-router';

@inject(MicroserviceService, Router)
export class MicroserviceDashboard {

  constructor(microserviceService, router){
    this.microserviceService = microserviceService;
    this.router = router;
    this.microservices = [];
  }

  attached(){
    this.microserviceService.getAllServices()
      .then((response) => {
        console.log(response)
        this.microservices = response
      });
  }

  goToService(serviceId){
    this.router.navigate('microservice/'+serviceId);
  }

}
