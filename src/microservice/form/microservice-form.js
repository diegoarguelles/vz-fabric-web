import {inject} from 'aurelia-framework';
import {MdToastService} from 'aurelia-materialize-bridge';
import {MicroserviceService} from '../services/microservice-service';
import {PlatformSettingsJava} from '../../model/platform-setting-java';
import {Microservice} from '../../model/microservice'

import {Router} from 'aurelia-router';

@inject(MicroserviceService, MdToastService, Router)
export class MicroserviceForm {

  showOptions = false;
  showNewAppOption= false;
  showLegacyAppOption= false;
  selectedPullOption = "";
  selectedBuildOption = "";
  selectedDeployEnvOption = "";
  selectedTestOption = [];

  testOptions = [
    {
      tool: "JUNIT",
      imgUrl: "http://www.codeaffine.com/wp-content/uploads/2016/02/junit-lambda-300x102.png"
    },
    {
      tool: "SELENIUM",
      imgUrl: "https://1.bp.blogspot.com/-QOT1qGdvL0Y/V-rv1MnVoNI/AAAAAAAAHa4/5hgkmQd_N7UysprgAaSKDADsv6yvyzInwCPcB/s200/Seleniumlogo.png"
    }
  ]

  selectServiceType(value){
    const FABRIC = 'FABRIC'
    const LEGACY = 'LEGACY'

    if(value == FABRIC){
      this.showOptions = true;
      this.showLegacyAppOption = false;
      this.showNewAppOption = true;
    }

    if(value == LEGACY){
      this.showOptions = true;
      this.showNewAppOption = false;
      this.showLegacyAppOption = true;
    }
  }

  constructor(microserviceService, toast, router) {
    this.router = router;
    this.microserviceService = microserviceService;
    this.toast = toast;
    this.showLoader = false;
    this.loaderValue = "0";
    this.loaderMessage = "In progress";
    this.showSpinner = false;
    this.microservice = {}
    this.microservice.version= '1.0.0-SNAPSHOT'
  }


  createPipelineString(){

    let pipelineArray = [];
    pipelineArray.push(this.selectedPullOption);
    pipelineArray.push(this.selectedBuildOption);
    pipelineArray.push(this.selectedDeployEnvOption);

    for (var i = 0; i < this.selectedTestOption.length; i++) {
      pipelineArray.push(this.selectedTestOption[i].tool)
    }

    let pipelineString = pipelineArray.join(',')
    return pipelineString;
  }

  saveService() {
    if (this.isValidMicroserviceInformation()) {
      let serviceToSave = {}

      serviceToSave.id = this.microservice.id;
      serviceToSave.vzId = "ARGUDI7";
      serviceToSave.replaceRepository = "0";
      serviceToSave.platform = 'JAVA';
      serviceToSave.projectType = this.microservice.serviceType;
      serviceToSave.legacyUrl = this.microservice.legacyUrl;
      serviceToSave.platformSettings = this.getPlatformSetting(this.microservice.serviceType)
      serviceToSave.pipeline = this.createPipelineString();

      this.showToast('Creating service...');

      this.showSpinner = true;

      this.microserviceService.saveService(new Microservice(serviceToSave))
        .then((response) => {
          if (!response) {
            this.showToast('An error occured');
          } else {
            let id = response.id
            this.showToast('Service created');
            this.showLoader = true;
            this.navigateTo('microservice/'+id)
          }
        });
    } else {
      this.showSpinner = false;
      this.showToast('Incomplete data');
    }

  }

  checkCreationStatus(microserviceId) {
    if (this.loaderValue == '100') {
      console.log('Redirecting....')
      this.router.navigate(`microservice/`+microserviceId);
    }
    this.microserviceService.getMicroserviceCreationStatus(microserviceId)
      .then((response) => {
      console.log('RESPONSE : '+response)
        if ('message' in response) {
          this.showToast('An error occured');
        } else {
          this.loaderValue = response.percentage.replace('%', '');
          this.loaderMessage = response.status
          setTimeout(null, 5000)
          this.checkCreationStatus(microserviceId);
        }
      });
  }


  isValidMicroserviceInformation() {
    if (this.microservice.serviceType == "JAVA") {
      return this.microservice.groupId !== '' && this.microservice.artifactId !== '' && this.microservice.version !== '' && this.microservice.serviceType !== '';
    }
    else {
      return this.microservice.legacyUrl !== '';
    }
  }

  getPlatformSetting(type) {
    let settings = {}
    if (type == 'FABRIC') {
      settings.groupId = this.microservice.groupId;
      settings.artifactId = this.microservice.artifactId;
      settings.version = this.microservice.version;
      // settings.jenkinsUrl = 'onejenkins.verizon.com/ves/';
      settings.jenkinsUrl = 'http://localhost:8080/';
      settings.jenkinsCredentials = {username: 'murruer', password: 'murruer'};
      settings.onestashUrl = "https://onestash.verizon.com";
      settings.onestashCredentials = {username: 'murruer', password: 'egipto2017.'};

      return new PlatformSettingsJava(settings)
    }

    if (type == 'LEGACY') {
      settings.artifactId = this.microservice.artifactId;
      settings.version = this.microservice.version;
      settings.jenkinsUrl = 'http://localhost:8080/';
      settings.jenkinsCredentials = {username: 'murruer', password: 'murruer'};
      settings.onestashUrl = this.microservice.legacyUrl;
      settings.onestashCredentials = {username: 'murruer', password: 'egipto2017.'};

      return new PlatformSettingsJava(settings)
    }
  }

  showToast(message) {
    this.toast.show(message, 4000);
  }

  navigateTo(route){
    this.router.navigate(route);
  }


  extractArtifactIdFromLegacyUrl(url){
    if(url){
      let urlSplitted = url.split('.git')
      console.log(urlSplitted)
      let locacion = urlSplitted[0];
      console.log(locacion)
      let locationSplitted = locacion.split('/')
      let project = locationSplitted[locationSplitted.length - 1]
      console.log(project)
      this.microservice.artifactId = project
    }
  }


}
