import {Router, RouterConfiguration} from 'aurelia-router';

export class App {

  constructor() {
    this.message = 'Hello World!';
  }

  configureRouter(config, router) {
    config.title = 'VZ Fabric';

    config.map([
      {
        route: ['', 'microservices'], name: 'microservices',
        moduleId: './microservice/dashboard/microservice-dashboard', nav: true, title: 'Microservices'
      },
      {
        route: 'microservice/create', name: 'newMicroservice',
        moduleId: './microservice/form/microservice-form', nav: true, title: 'New Microservice'
      },
      {
        route: 'microservice/:serviceId', name: 'Microservice',
        moduleId: './microservice/view/microservice-view',  title: 'Microservice'
      },
    ]);

    this.router = router;
  }

}
